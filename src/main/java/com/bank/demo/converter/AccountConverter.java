package com.bank.demo.converter;

import com.bank.demo.model.Account;
import com.bank.demo.dto.AccountDetailsDto;
import com.bank.demo.dto.AccountDto;
import org.springframework.stereotype.Component;


@Component
public class AccountConverter {

    public AccountDto toDto(Account account) {
        AccountDto accountDto = new AccountDto();
        accountDto.setFirstName(account.getFirstName());
        accountDto.setLastName(account.getLastName());
        accountDto.setAddress(account.getAddress());
        return accountDto;
    }

    public static AccountDetailsDto toAccountDetailsDto(Account account) {
        AccountDetailsDto accountDetailsDto = new AccountDetailsDto();
        accountDetailsDto.setAccountNo(account.getAccountNo());
        accountDetailsDto.setAddress(account.getAddress());
        accountDetailsDto.setFirstName(account.getFirstName());
        accountDetailsDto.setLastName(account.getLastName());
        return accountDetailsDto;
    }

    public static Account toAccount(Account account,AccountDto accountDto) {
        account.setFirstName(accountDto.getFirstName());
        account.setLastName(accountDto.getLastName());
        account.setAddress(accountDto.getAddress());
        return account;
    }
    public static Account toAccount(AccountDto accountDto) {
        Account account=new Account();
        account.setAccountNo(account.getAccountNo());
        account.setFirstName(accountDto.getFirstName());
        account.setLastName(accountDto.getLastName());
        account.setAddress(accountDto.getAddress());
        return account;
    }
}
