package com.bank.demo.controller;

import com.bank.demo.dto.AccountDetailsDto;
import com.bank.demo.dto.AccountDto;
import com.bank.demo.repository.BankRepository;
import com.bank.demo.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;

@RestController
public class BankController {
    @Autowired
    private BankService bankService;
    @Autowired
    BankRepository bankRepository;

    @GetMapping("/accounts")
    public List<AccountDetailsDto> getAllDetails() {

        return bankService.getAllAccount();
    }

    @GetMapping("/account/{id}")
    public ResponseEntity<AccountDetailsDto> getById(@PathVariable("id") Long id) {

        return bankService.getAccountById(id);
    }

    @PostMapping("/account")
    public ResponseEntity<AccountDto> create(@RequestBody AccountDto accountDto) {
        return bankService.createAccount(accountDto);
    }

    @PutMapping("account/{id}")
    public ResponseEntity<AccountDto> update(@PathVariable("id") Long id, @RequestBody AccountDto account) {
        return bankService.updateAccount(id, account);
    }

    @DeleteMapping("account/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") Long id) {

        return bankService.deleteById(id);
    }

    @DeleteMapping("/accounts")
    public ResponseEntity<HttpStatus> deleteAll() {

        return bankService.deleteDetails();
    }
}


