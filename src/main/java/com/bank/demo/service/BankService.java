package com.bank.demo.service;

import com.bank.demo.dto.AccountDetailsDto;
import com.bank.demo.dto.AccountDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface BankService {
    public ResponseEntity<AccountDetailsDto> getAccountById(Long id);

    public ResponseEntity<AccountDto> createAccount(AccountDto account);

    public ResponseEntity<AccountDto> updateAccount(Long id, AccountDto account);

    public ResponseEntity<HttpStatus> deleteById(Long id);

    public ResponseEntity<HttpStatus> deleteDetails();

    public List<AccountDetailsDto> getAllAccount();
}
