package com.bank.demo.service;
import com.bank.demo.model.Account;
import com.bank.demo.dto.AccountDetailsDto;
import com.bank.demo.dto.AccountDto;
import com.bank.demo.repository.BankRepository;
import com.bank.demo.converter.AccountConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BankServiceImpl implements BankService {
    @Autowired
    private BankRepository bankRepository;
    @Autowired
    private AccountConverter accountConverter;

    public ResponseEntity<AccountDetailsDto> getAccountById(Long id) {
        return new ResponseEntity<>(bankRepository.findById(id).map(AccountConverter::toAccountDetailsDto).orElseThrow(()-> new IllegalArgumentException("Account not exists")), HttpStatus.OK);
    }

    public ResponseEntity<AccountDto> createAccount(AccountDto account) {
        try {
            bankRepository.save(accountConverter.toAccount(account));
            return new ResponseEntity<>(account, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    public ResponseEntity<AccountDto> updateAccount(Long id, AccountDto account) {
        bankRepository.save(bankRepository.findById(id).map(sampleAccount ->AccountConverter.toAccount(sampleAccount,account)).orElseThrow(()-> new IllegalArgumentException("Account not exists")));
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    public ResponseEntity<HttpStatus> deleteById(Long id) {

        bankRepository.delete(bankRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Account not exists")));

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<HttpStatus> deleteDetails() {
        try {
            bankRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<AccountDetailsDto> getAllAccount() {
        List<Account> accountList = bankRepository.findAll();
        List<AccountDetailsDto> accountDetailsDtoList = new ArrayList<>();
        for (Account account : accountList) {
            accountDetailsDtoList.add(accountConverter.toAccountDetailsDto(account));
        }
        return accountDetailsDtoList;
    }

}
