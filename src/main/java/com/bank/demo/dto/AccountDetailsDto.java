package com.bank.demo.dto;

import com.bank.demo.model.Address;

import java.math.BigInteger;

public class AccountDetailsDto {

    private String firstName;

    private String lastName;

    private BigInteger accountNo;

    private Address address;

    public AccountDetailsDto() {
    }

    public AccountDetailsDto(String firstName, String lastName, BigInteger accountNo, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNo = accountNo;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigInteger getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(BigInteger accountNo) {
        this.accountNo = accountNo;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
